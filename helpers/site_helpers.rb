module SiteHelpers

	def page_title
		title = "Set your site title in /helpers/site_helpers.rb"
		if data.page.title
			title = data.page.title + " - " + title
		end
		title
	end
	
	def page_description
		if data.page.description
			description = data.page.description
		else
			description = "Set your site description in /helpers/site_helpers.rb"
		end
		description
	end

	def page_class
		slug = ""
		if data.page.slug
			slug = "view-" + data.page.slug
		end
		slug
	end

end